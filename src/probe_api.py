"""
VERSION: Prototype - 0.2.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""
from urllib.parse import urljoin

import requests


def submit_measurement(api: str, target: str, ip_version: int, sample: dict):
    response = requests.post(
        urljoin(api, f"record/v{ip_version}/{target}"),
        json=sample,
    )
    response.raise_for_status()
