"""
VERSION: Prototype - 0.1.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2021
"""

from random import choice
from concurrent.futures import ThreadPoolExecutor, as_completed
from typing import cast, List, Dict, Tuple

import click
from protocol import Measurement

from backend_api import get_metrics, Beacon
from probe_api import submit_measurement


@click.command()
@click.option(
    "--backend-api",
    envvar="BACKEND_API",
    prompt="Backend API URL",
    help="The URL of the Backend (e.g. http://backend-example.latency.gg)",
)
@click.option(
    "--probe-api",
    envvar="PROBE_API",
    prompt="Probe API URL",
    help="The URL of the Probe API (e.g. https://probe.latency.gg)",
)
def main(
    backend_api: str,
    probe_api: str,
):
    source, clean_metrics, stale_metrics = get_metrics(backend_api)
    measurements = prepare_measurements(stale_metrics, source, probe_api)
    final_metrics = run(clean_metrics, measurements, source)
    for provider_name, location in final_metrics.items():
        print(f"{provider_name}:")
        for location_name, metrics in location.items():
            print(
                f"    {location_name}:\n"
                f"        latency: {metrics['rtt']}\n"
                f"        jitter: {metrics['stddev']}"
            )
    failed_measurements = []
    for provider_name in stale_metrics:
        for location_name in stale_metrics[provider_name]:
            if location_name not in final_metrics.get(provider_name, {}):
                failed_measurements.append(f"{provider_name} - {location_name}")
    if failed_measurements:
        print("failed to measure:")
        print(",\n".join(failed_measurements))


def run(
    clean_metrics: Dict[str, Dict[str, Dict]],
    measurements: List[Tuple[str, str, Measurement]],
    source: Dict,
) -> Dict[str, Dict[str, Dict]]:
    with ThreadPoolExecutor(max_workers=8) as executor:
        measurement_futures = {
            executor.submit(measurement[2]): (measurement[0], measurement[1])
            for measurement in measurements
        }
        for future in as_completed(measurement_futures):
            target = measurement_futures[future]
            try:
                measurement = future.result()
                if measurement.failed:
                    continue
                measurement_dict = measurement.as_dict()
                submit_measurement(
                    measurement.probe_api,
                    measurement.target,
                    cast(int, source["version"]),
                    measurement_dict,
                )
                clean_metrics[target[0]][target[1]] = measurement_dict
            except Exception as exc:
                print(f"{target} generated an exception: {exc}")
    return clean_metrics


def prepare_measurements(
    stale_metrics: Dict[str, Dict[str, Dict]], source: Dict, probe_api: str
) -> List[Tuple[str, str, Measurement]]:
    measurements = []
    for provider_name, location in stale_metrics.items():
        for location_name, metrics in location.items():
            if len(cast(List[Beacon], metrics["beacons"])) == 0:  # nomypy
                continue
            measurements.append(
                (
                    provider_name,
                    location_name,
                    Measurement(
                        source["addr"],
                        choice(cast(List[Beacon], metrics["beacons"]))[  # nosec
                            f"ipv{source['version']}"
                        ],
                        source["ident"],
                        probe_api,
                    ),
                )
            )
    return measurements
