import socket

from enum import Enum
from functools import partial
from ipaddress import ip_address
from random import randint
from select import select
from datetime import datetime
from statistics import mean, stdev

from statemachine import StateMachine, State  # type: ignore

from wire_formats import (
    EchoClientServer,
    EchoServerClient,
    EchoClientServerType,
    EchoServerClientType,
)


MILLISECONDS = 1000  # Convert from seconds to milliseconds


class Valid(Enum):
    Unknown = -1
    Invalid = 0
    Valid = 1


class Measurement:
    def __init__(self, own_ip, target, ident, probe_api):
        self.version = "v0.0.0-local"
        self.ip = own_ip
        self.target = target
        self.ident = ident.encode()
        self.seq = randint(0, 255)  # nosec
        self.probe_api = probe_api
        if ip_address(self.target).version == 4:
            self.port = 9998
            af = socket.AF_INET
        else:
            self.port = 9999
            af = socket.AF_INET6
        self.af = af
        self.state = MeasurementState()  # pylint: disable=no-value-for-parameter
        self.timestamp_c0 = 0
        self.timestamp_c1 = 0
        self.timestamp_c2 = 0
        self.timestamp_c3 = 0
        self.timestamp_s0 = 0
        self.timestamp_s1 = 0
        self.timestamp_s2 = 0
        self.signatures = []
        self.errors = 0
        self.threshold = 3

    def __call__(self) -> "Measurement":
        self.sock = socket.socket(self.af, socket.SOCK_DGRAM)
        self.sock.sendto(bytes(self.generate_packet()), (self.target, self.port))
        while not self.state.is_complete and self.errors < self.threshold:
            rlist, _, _ = select([self.sock], [], [], 0.5)
            if self.sock in rlist:
                data, (incoming_ip, _) = self.sock.recvfrom(128)
                valid = self.is_valid(data, incoming_ip)
                if valid == valid.Invalid:
                    self.errors += 1
                elif valid == valid.Unknown:
                    continue
                else:
                    packet = EchoServerClient.from_buffer_copy(data)
                    self.process_incoming(packet)
            else:
                self.errors += 1
            self.sock.sendto(bytes(self.generate_packet()), (self.target, self.port))
        return self

    @property
    def failed(self):
        return self.errors >= self.threshold

    def is_valid(self, data, incoming_ip) -> Valid:
        valid = Valid.Valid
        if len(data) != 112:
            if incoming_ip == self.target:
                valid = Valid.Invalid
            else:
                valid = Valid.Unknown
        return valid

    def process_incoming(self, packet: EchoServerClient):
        self.signatures.append(packet.signature.decode())
        if packet.seq != self.seq:
            self.errors += 1
            print("not my packet")
        if packet.type == EchoServerClientType.Initial and self.state.is_c0:
            self.timestamp_s0 = packet.timestamp
            self.state.recv_s0()
        elif packet.type == EchoServerClientType.Next and self.state.is_c1:
            self.timestamp_s1 = packet.timestamp
            self.state.recv_s1()
        elif packet.type == EchoServerClientType.Final and self.state.is_c2:
            timestamp = int(datetime.now().timestamp() * MILLISECONDS)
            self.timestamp_s2 = packet.timestamp
            self.timestamp_c3 = timestamp
            self.state.recv_s2()

    def generate_packet(self):
        timestamp = int(datetime.now().timestamp() * MILLISECONDS)
        packet_type = EchoClientServerType.Initial
        if self.state.is_c0:
            self.timestamp_c0 = timestamp
        elif self.state.current_state in (MeasurementState.s0, MeasurementState.c1):
            if self.state.is_s0:
                self.state.send_c1()
            packet_type = EchoClientServerType.Second
            self.timestamp_c1 = timestamp
        elif self.state.current_state in (MeasurementState.s1, MeasurementState.c2):
            if self.state.is_s1:
                self.state.send_c2()
            packet_type = EchoClientServerType.Final
            self.timestamp_c2 = timestamp
        return EchoClientServer(
            version=2,
            type=packet_type,
            seq=self.seq,
            ident=self.ident,
            timestamp=timestamp,
        )

    def as_dict(self):
        raw_rtts = [
            self.timestamp_c1 - self.timestamp_c0,
            self.timestamp_c2 - self.timestamp_c1,
            self.timestamp_c3 - self.timestamp_c2,
            self.timestamp_s1 - self.timestamp_s0,
            self.timestamp_s2 - self.timestamp_s1,
        ]
        p_max = partial(max, 0)
        p_min = partial(min, 500)
        corrected_rtts = list(map(p_max, map(p_min, raw_rtts)))
        sample = {
            "type": "udp-data",
            "version": self.version,
            "ip": self.ip,
            "ident": self.ident.decode(),
            "rtt": int(mean(corrected_rtts)),
            "stddev": int(stdev(corrected_rtts)),
            "raw": {
                "timestamp_c0": self.timestamp_c0,
                "timestamp_c1": self.timestamp_c1,
                "timestamp_c2": self.timestamp_c2,
                "timestamp_c3": self.timestamp_c3,
                "timestamp_s0": self.timestamp_s0,
                "timestamp_s1": self.timestamp_s1,
                "timestamp_s2": self.timestamp_s2,
                "signatures": self.signatures,
                "errors": self.errors,
            },
        }
        return sample


class MeasurementState(StateMachine):
    c0 = State("C0", initial=True)  # pylint: disable=unexpected-keyword-arg
    s0 = State("S0")
    c1 = State("C1")
    s1 = State("S1")
    c2 = State("C2")
    complete = State("complete")

    recv_s0 = c0.to(s0)
    send_c1 = s0.to(c1)
    recv_s1 = c1.to(s1)
    send_c2 = s1.to(c2)
    recv_s2 = c2.to(complete)
